# 1stCube
This project contains the overviewpage of the 1stCube: https://cubecobra.com/cube/overview/1stcube
It descirbes:
- goal of the cube 
- the design pattern it follows,
- an overview of the color synergies 
- supported archetypes 

The goal is to give new players and overview of the cube. And provides a helping hand in the preparation for the next cube event. 
